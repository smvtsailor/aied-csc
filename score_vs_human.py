# This script runs analyses to compare the output of the aIED detector with scoring by a
# trained neurophysiologist.

import os
import re
from xml.etree.ElementPath import prepare_self
import numpy as np
import pandas as pd
import pathlib
import logging
import scipy.io
import matplotlib as mpl
import matplotlib.pyplot as plt
import neuralynx as nlx
import pathlib

mpl.use("Agg")

AIED_SCORE_FILE = "data/sessions.csv"

HUMAN_SCORE_FILE_TEMPLATE = (
    "data/labeled_ied_macros/events/{session}/IED1/eventsIED.mat"
)

AUTHORITATIVE_SCORE_BASEDIR = "data/labeled_ied_macros/events"
AUTHORITATIVE_SCORE_SESS_FILE = "IED1/eventsIED.mat"

MATCH_MARGIN_MS = 100

session_regex = re.compile(r"(?P<full_sess_id>(?P<subj_id>P\d{1,3}CS)_\d{6})")
chan_regex = re.compile(r"[a-zA-Z]*(?P<chan_num>\d{1,3})\.ncs")


def evaluate_all_sessions():
    """
    Evaluate the performance of the aIED detector in all available sessions, using human
    scoring as a authoratative detector.

    """
    auth_results = load_authoritative_results()
    ai_results = load_ai_results()

    # Get a list of the sessions from both sources, because it is possible that the one
    # of the sources didn't detect any IEDs in a session.
    all_sessions = set(ai_results.session.unique()).union(
        set(auth_results.session.unique())
    )

    correspondence = []
    for session in all_sessions:
        sess_ai_results = ai_results.query(f'session == "{session}" and is_valid == 1')
        sess_auth_results = auth_results[auth_results.session == session]
        sess_correspondence = compute_correspondence(
            sess_ai_results,
            sess_auth_results,
            MATCH_MARGIN_MS,
        )
        sess_correspondence.loc[:, "session"] = session
        correspondence.append(sess_correspondence)

    correspondence = pd.concat(correspondence)

    confusion = correspondence.groupby("session").apply(confusion_matrix)
    print(confusion)
    confusion.to_csv("results/score_vs_human.csv")

    return correspondence


def plot_confusion_vs_match_margin(min_match_ms=0, max_match_ms=3000, steps=51):
    """
    Make a plot of the precision, recall, and F1 score vs the matching threshold.

    """

    # Define a filepath for where to save the image
    out_path = "results/confusion_vs_match.png"

    # Calculate the values of match_ms that we will use
    match_thresholds = np.linspace(min_match_ms, max_match_ms, steps)

    auth_results = load_authoritative_results()
    ai_results = load_ai_results()

    # Get a list of the sessions from both sources, because it is possible that the one
    # of the sources didn't detect any IEDs in a session.
    all_sessions = set(ai_results.session.unique()).union(
        set(auth_results.session.unique())
    )

    confusions = {}
    for match_thresh in match_thresholds:

        correspondence = []
        for session in all_sessions:
            sess_ai_results = ai_results.query(
                f'session == "{session}" and is_valid == 1'
            )
            sess_auth_results = auth_results[auth_results.session == session]
            sess_correspondence = compute_correspondence(
                sess_ai_results.timestamp.values,
                sess_auth_results.timestamps.values,
                match_thresh,
            )
            sess_correspondence.loc[:, "session"] = session
            correspondence.append(sess_correspondence)

        correspondence = pd.concat(correspondence)
        confusion = confusion_matrix(correspondence)
        confusions[match_thresh] = confusion
    confusions = pd.DataFrame(confusions).T

    # Make the plot
    fig = mpl.figure.Figure(figsize=(7, 3.5))
    ax = fig.add_subplot(1, 1, 1)

    ax.plot(confusions.index.values, confusions.precision.values, label="Precision")
    ax.plot(confusions.index.values, confusions.recall.values, label="Recall")
    ax.plot(confusions.index.values, confusions.f1_score.values, label="F1 Score")

    ax.set_ylim([0, 1])
    ax.legend()
    fig.savefig(out_path)


def compute_correspondence(ai_results, auth_results, margin_ms):
    """
    Evaluate the performance of the AI IED detector with respect to an authoratative
    listing of IED timestamps.

    """
    # We will go through each list of timestamps, and for each one
    ai_results = ai_results.sort_values("timestamp").reset_index(drop=True)
    auth_results = auth_results.sort_values("timestamps").reset_index(drop=True)
    ai_timestamps = ai_results.timestamp.values

    ai_cum_matches = np.full(ai_results.shape[0], False)
    margin_us = margin_ms * 1e3

    correspondence = pd.DataFrame(
        {
            "timestamp": auth_results.timestamps.values,
            "real": True,
            "detected": False,
            "ai_matches": None,
        }
    )
    correspondence.set_index("timestamp", inplace=True)

    # Loop over the authoratative timestamps and find whether there is at least one
    # match in ai_ts. There might be multiple matches because the AI detector runs per
    # channel, and the human rater rates all channels at once.
    for auth_ts in auth_results.timestamps:

        # Find AI detections that overlapped with this human detection
        matches = (ai_timestamps < auth_ts + margin_us) & (
            ai_timestamps > auth_ts - margin_us
        )

        # Note that these AI detections appeared in the human detections
        ai_cum_matches = ai_cum_matches | matches

        # Note whether any AI detections
        if np.sum(matches) > 0:
            correspondence.loc[auth_ts, "detected"] = True
            ai_matches = ai_results.loc[matches, ["timestamp", "path"]]
            correspondence.at[auth_ts, "ai_matches"] = ai_matches

    unmatched = ai_timestamps[~ai_cum_matches]
    diff = np.diff(unmatched)
    diff = np.insert(diff, 0, 0)
    deduped = unmatched[diff > margin_us]
    unmatched_df = pd.DataFrame({"timestamp": deduped, "real": False, "detected": True})
    unmatched_df.set_index("timestamp", inplace=True)
    correspondence = pd.concat([correspondence, unmatched_df])

    return correspondence


def confusion_matrix(correspondence):

    tp = ((correspondence.real == True) & (correspondence.detected == True)).sum()
    fp = ((correspondence.real == False) & (correspondence.detected == True)).sum()
    fn = ((correspondence.real == True) & (correspondence.detected == False)).sum()

    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1_score = 2 * tp / (2 * tp + fp + fn)

    return pd.Series(
        {
            "tp": tp,
            "fp": fp,
            "fn": fn,
            "precision": precision,
            "recall": recall,
            "f1_score": f1_score,
        }
    )


def load_authoritative_results():
    """
    Walk through the directory of human-generated IED scores and load them into a
    DataFrame.

    """
    timestamps = []
    for dirname in os.listdir(AUTHORITATIVE_SCORE_BASEDIR):

        sess_basepath = os.path.join(AUTHORITATIVE_SCORE_BASEDIR, dirname)
        if not os.path.isdir(sess_basepath):
            continue

        sess_match = session_regex.match(dirname)
        if sess_match:
            subj_id = sess_match.group("subj_id")
            sess_id = sess_match.group("full_sess_id")
        else:
            logging.warning(f"Could not process directory with name {sess_basepath}")
            continue

        # If we've made it here, we have a scored session! Load the mat file and get the
        # timestamps into a Series.
        sess_mat_path = os.path.join(sess_basepath, AUTHORITATIVE_SCORE_SESS_FILE)
        sess_mat = scipy.io.loadmat(sess_mat_path)["events"]
        sess_ts = pd.DataFrame(
            {"timestamps": sess_mat[:, 0], "session": sess_id, "subject": subj_id}
        )
        timestamps.append(sess_ts)

    return pd.concat(timestamps)


def load_ai_results():
    """
    Retrieve a DataFrame of session info with columns (session_name, channel_name,
    human_review_path, nsc_path)

    """

    # Load the aIED score file so that we can get a list of sessions that we used
    ai_detections = pd.read_csv(AIED_SCORE_FILE)
    ai_detections.rename({"session": "sess_path"}, axis=1, inplace=True)

    # Examine the paths to determine the subject, channel, and session
    ai_sess_info = ai_detections.path.apply(session_channel_from_ncs_path)

    # Merge the fields that we extracted back into the IED detections dataframe
    combined_info = pd.concat([ai_detections, ai_sess_info], axis=1)

    return combined_info


def session_channel_from_ncs_path(path):
    """
    Extract the session ID and a channel number from a path to an NCS file. Makes a lot
    of assumptions.

    """
    if "\\" in path:
        fixed_path = pathlib.PureWindowsPath(path).as_posix()
    else:
        fixed_path = path
    purepath = pathlib.PurePath(fixed_path)

    subj_id = None
    sess_id = None
    chan_num = None
    for path_part in purepath.parts:

        # Extract the subject ID and session ID
        sess_match = session_regex.match(path_part)
        if sess_match:
            subj_id = sess_match.group("subj_id")
            sess_id = sess_match.group("full_sess_id")

        # Extract the channel number
        chan_match = chan_regex.match(path_part)
        if chan_match:
            chan_num = chan_match.group("chan_num")

    if subj_id is None or sess_id is None or chan_num is None:
        raise ValueError("Could not locate fields within path")

    return pd.Series({"channel": chan_num, "subject": subj_id, "session": sess_id})


def plot_ieds(correspondence, out_dir, num_tp=10, num_fp=10, num_fn=10, shuffle=False):
    """
    Plot a selection of IEDs - true positives, false positives, and false negatives -
    from the IED detector output and save them as images to a specified directory.

    Parameters
    ----------
    correspondence: pd.DataFrame
        A listing of which IEDs were detected by the human and automated detector

    out_dir: str
        A path to a directory in which images can be saved

    num_tp: int, optional
        The maximum number of images of true positive IEDs to generate.

    num_fp: int, optional
        The maximum number of images of false positive IEDs to generate.

    num_fn: int, optional
        The maximum number of images of false negative IEDs to generate.

    shuffle: bool, optional
        If true, IEDs will be chosen randomly if there are more options than the
        requested number of images. Otherwise, the IEDs will be used sequentially until
        each quota is met.

    """

    # Get the true positives
    tp = correspondence[correspondence["real"] & correspondence["detected"]]
    num_tp = min(len(tp), num_tp)
    if shuffle:
        tp_sample = tp.sample(num_tp)
    else:
        tp_sample = tp.iloc[0:num_tp, :]

    # Get the false positives
    fp = correspondence[~correspondence["real"] & correspondence["detected"]]
    num_fp = min(len(tp), num_fp)
    if shuffle:
        fp_sample = fp.sample(num_fp)
    else:
        fp_sample = fp.iloc[0:num_fp, :]

    # Plot the true positives
    for timestamp, ied in tp_sample.iterrows():
        path = os.path.join(out_dir, "true_pos", f"{ied.session}_{timestamp}")
        plot_ied(timestamp, ied, path)


def plot_ied(timestamp, ied, path, pre_ms=1000, post_ms=1000):

    eegs = []
    starts = []

    clip_start = timestamp - pre_ms
    clip_end = timestamp + post_ms

    for _, ai_match in ied.ai_matches.iterrows():
        fixed_path = os.path.join(*pathlib.PureWindowsPath(ai_match.path).parts)
        try:
            eeg, headers = nlx.load_ncs_for_sorting(fixed_path)
        except IndexError as exc:
            print(f"File {ai_match.path} is invalid.")
            continue
        eeg_clip = eeg.loc[clip_start:clip_end]
        eeg_clip.index = eeg_clip.index - timestamp
        eegs.append(eeg_clip)
        starts.append(ai_match.timestamp - timestamp)

    fig = plt.figure(figsize=(3.5, 3.5))
    n_chans = len(eegs)
    n_rows = n_chans + 2

    # Set up a gridspec for the EEG subplots
    ecog_gs = mpl.gridspec.GridSpec(
        nrows=n_chans, ncols=1, left=0.05, right=0.95, top=0.95, bottom=0.15, hspace=0
    )

    # Plot the ecog segments on their own subplots
    axs = _plot_eeg(eeg=eegs, fig=fig, gs=ecog_gs, starts=starts)

    fig.savefig(f"{path}.svg", transparent=True)
    fig.savefig(f"{path}.png", dpi=600, transparent=True)
    plt.close(fig)


def _plot_eeg(eeg, fig, gs, starts):
    """
    Plots EEG on the provided figure as a series of subplots with a shared x
    axis and linked y axes. Returns a list of axes.

    :param eeg: A section of an EEG dataframe

    :param fig: A figure to use for plotting

    :param gs: A grid spec to use for plotting. Should have nrows equal to the
               number of channels in the EEG

    :returns: A list of matplotlib axes that were used for the EEG plot

    """

    # Define the colors for the different parts of the EEG
    PAD_COLOR = "#9e9e9e"
    SURROUND_COLOR = "#003e82"
    SPIKE_COLOR = "#820002"

    PRE_C_DARK = "#007DD9"
    PRE_C_LIGHT = "#C2F5FF"
    SPIKE_C_DARK = "#C94945"
    SPIKE_C_LIGHT = "#FFD9D8"
    POST_C_DARK = "#008D00"
    POST_C_LIGHT = "#B5FFB8"

    # Get a list of channels
    num_chans = len(eeg)

    # Find the extents of the x axis
    xmin = eeg.index.min()
    xmax = eeg.index.max()

    # Make a list of axes
    axs = []

    # Place to store the shared axes
    share_ax = None

    # Loop through the channels and make a subplot for each
    for i, chan_eeg in enumerate(eeg):

        chan_start = starts[i]

        # Make the subplot for the current channel
        ax = fig.add_subplot(gs[i], sharex=share_ax, sharey=share_ax)

        axs.append(ax)

        # Store the shared axes if we haven't done that yet
        if not share_ax:
            share_ax = ax

        ax.plot(chan_eeg, color=SURROUND_COLOR, linewidth=0.85)

        xticks = list(range(xmin + 1000, xmax, 1000))
        xticklabels = [int((x) / 1000) for x in xticks]
        ax.set_xticks(xticks, minor=False)
        ax.grid(
            b=True, axis="x", which="major", linestyle=":", linewidth=0.5, c="#353535"
        )
        ax.set_xlim(left=chan_eeg.index.values.min(), right=chan_eeg.index.values.max())
        ax.set_ylim(bottom=chan_eeg.values.min(), top=chan_eeg.values.max())
        ax.set_ylabel(chan)
        ax.set_xlabel("Time relative to authoratative rating (sec)", fontsize=9)
        ax.set_frame_on(False)
        ax.set_xticklabels(xticklabels)

        ax.set_yticks([])
        ax.set_ylabel("")
        ax.tick_params(length=0, labelsize=7)
        ax.label_outer()

        # Get the ymin and ymax for plotting highlight patches
        ymin = chan_eeg.values.min()
        ymax = chan_eeg.values.max()
        h = ymax - ymin

    return axs


if __name__ == "__main__":

    correspondence = evaluate_all_sessions()
    plot_ieds(correspondence, "results/ied_plots")
    plot_confusion_vs_match_margin()
