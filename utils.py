import scipy
import numpy as np
import pandas as pd
import matplotlib as mpl

import os


def butter_highpass(low_limit, samp_freq, order=4):
    """
    Returns a Butterworth highpass filter with the specified characteristics.

    """
    nyquist_limit = samp_freq / 2
    low_prop = low_limit / nyquist_limit
    sos = scipy.signal.butter(order, low_prop, btype="high", output="sos")

    def bb_filter(data):
        return scipy.signal.sosfiltfilt(sos, data)

    return bb_filter


def mains_filter(ecog, samp_freq, max_freq=None):
    """
    Applies IIR notch filters to remove power line artifacts.

    :param ecog: A dataframe containing ECoG data, with an index in ms and
                 columns for each channel.

    :param samp_freq: The sampling frequency in Hz.

    :param max_freq: The maximum frequency to filter out. Useful if you plan to
                     downsample after filtering.

    :returns: A dataframe with the same structure as ecog containing the
              filtered data.

    """

    MAINS_FREQ = 60
    Q_FACTOR = 60

    if not max_freq:
        max_freq = samp_freq / 2

    # Calculate the frequencies that we have to filter at up to the Nyquist
    # limit
    filter_freqs = np.arange(MAINS_FREQ, max_freq, MAINS_FREQ)

    # Compute the parameters for each filter, and then combine them using
    # convolution.
    combined_b = None
    combined_a = None
    for center in filter_freqs:
        b, a = scipy.signal.iirnotch(center, Q_FACTOR, samp_freq)
        if combined_b is not None:
            combined_b = np.convolve(combined_b, b)
            combined_a = np.convolve(combined_a, a)
        else:
            combined_b = b
            combined_a = a

    # Apply the filter
    filtered_ecog = pd.Series(
        scipy.signal.filtfilt(combined_b, combined_a, ecog.values), index=ecog.index
    )

    return filtered_ecog


def downsample_with_gaps(eeg, orig_sf, new_sf):
    """
    Downsample an eeg clip that contains gaps.

    Parameters
    ----------
    eeg: pd.Series
        A series containing ECoG data from a single channel with timestamps in the index.

    orig_sf: float
        The number of samples per second in the passed ECoG series.

    new_sf: float
        The number of samples per second in the output ECoG series

    """

    # Define how much inter-sample jitter is acceptable
    jitter_tolerance = 0.1

    # Figure out where there are gaps in the timeline
    idx_diff = np.diff(eeg.index)
    gap_idx = (
        np.abs((idx_diff - np.median(idx_diff)) / np.median(idx_diff))
        > jitter_tolerance
    )
    gaps = np.where(gap_idx)
    # Add a dummy gap at the end of the array so that we can capture the last segment
    gaps = np.append(gaps, len(eeg) - 1)

    # Store the downsampled segments of EEG
    segments = []

    # Keep track of the previous gap start index
    seg_start = 0
    for gap in gaps:
        orig_segment = eeg.iloc[seg_start:gap]
        orig_idx = orig_segment.index
        new_num_samples = int(np.around(len(orig_segment) * new_sf / orig_sf))
        new_segment = scipy.signal.resample(orig_segment, new_num_samples)

        # Figure out the new time index by dividing the total time of the recording by the
        # number of samples
        new_idx = np.linspace(orig_idx.values[0], orig_idx.values[-1], new_num_samples)

        resampled_segment = pd.Series(new_segment, index=new_idx)
        segments.append(resampled_segment)
        seg_start = gap + 1

    final_resampled = pd.concat(segments)

    return final_resampled


def preprocess(ecog, samp_freq):
    """
    Do the preprocessing steps for aIED as performed in the aIED paper.

    Parameters
    ----------
    ecog: pd.Series
        A series containing ECoG data from a single channel.

    samp_freq: float
        The number of samples per second in the passed ECoG series.

    Returns
    -------
    pd.Series
        Preprocessed ECoG data from a single channel.

    """
    index = ecog.index
    # Filter the data to remove spectral content outside of [1,250] Hz
    sos = scipy.signal.butter(4, [1, 250], btype="bandpass", output="sos", fs=samp_freq)
    ecog = scipy.signal.sosfiltfilt(sos, ecog)

    # Filter out mains hum
    ecog = mains_filter(pd.Series(ecog, index=index), samp_freq)

    # Downsample the data to 200Hz.
    ecog = downsample_with_gaps(ecog, samp_freq, 200)

    return ecog


def plot_detections(
    eeg, samp_freq, sample_numbers, base_filename, base_dir, margin_sec=1
):
    """
    Plot clips of EEG that correspond to IEDs.

    Parameters
    ----------
    eeg: pd.Series
        A single time series containing EEG data

    samp_freq: float
        The number of samples per second in the time series

    sample_numbers:  list
        A list of start times of detected IEDs

    base_filename: str
        The start of the filenames for the outputted images. The sample number will be
        appended to this text to form the filename.

    base_dir: str
        The directory in which to store the outputted images.

    margin_sec: int
        The number of seconds on each side of the detection to include in the plot.

    """

    for samp_num in sample_numbers:
        margin_samps = margin_sec * samp_freq
        # Extract the clip of EEG data
        min_idx = max(0, samp_num - margin_samps)
        max_idx = min(len(eeg), samp_num + margin_samps)
        clip = eeg.iloc[min_idx:max_idx]

        fig = mpl.figure.Figure(figsize=(3.5, 3.5))
        ax = fig.add_subplot(1, 1, 1)

        ax.axvline(x=samp_num, c="r")
        ax.plot(clip.index, clip.values)

        # Even if the full clip isn't available, keep the IED centered in the plot.
        ax.set_xlim([samp_num - margin_samps, samp_num + margin_samps])

        path = os.path.join(base_dir, base_filename + str(samp_num)) + ".png"
        fig.savefig(path)
