import detectIEDs
import pandas as pd
import numpy as np
import matplotlib as mpl
import spikedetector
import aidetect
import utils


def load_sample_data():

    sample_source = "sample_eegdata.csv"
    sample_eeg = np.genfromtxt(sample_source, delimiter=",", autostrip=True)
    sample_eeg = pd.DataFrame(sample_eeg.T)

    sample_label_file = "sample_finalspikes.csv"
    sample_labels = pd.read_csv(sample_label_file)

    return sample_eeg, sample_labels


def find_sample_spikes(eeg, samp_rate):

    all_ieds = {}
    for chan in eeg.columns:
        chan_eeg = eeg.loc[:, chan]

        # Make sure that we're passing the same thing as the reference implementation
        compare_to_notebook_predetect(chan_eeg, chan, samp_rate)

        # Run template matching detection
        prospectives = spikedetector.detect.detect(chan_eeg, samp_rate)
        prospective_starts = [x[0] for x in prospectives]

        # compare_to_notebook_detections(prospective_starts, chan)
        # utils.plot_detections(
        #     chan_eeg,
        #     samp_rate,
        #     prospective_starts,
        #     f"chan{chan}_",
        #     "intermediate",
        #     margin_sec=1,
        # )

        # Run the AI based component of the spike detector
        ieds = aidetect.aidetect(chan_eeg, prospective_starts, samp_rate)
        print(f"Found {ieds.is_valid.sum()} IEDs in channel {chan}")

        all_ieds[chan] = ieds

    return pd.concat(all_ieds)


def compare_to_notebook_predetect(eeg, chan_number, samp_freq):
    """
    Compare the inputs to the template detector between the aIED ipython notebook and
    this implementation.

    """
    # Plot the first 10 seconds of the data
    range_max = samp_freq * 10

    # Get the path to the record of EEG that was fed into the detector in the notebook,
    # and load the CSV data.
    authoritative_path = f"intermediate/chan_{chan_number}_predetection.csv"
    authoritative_eeg = pd.read_csv(authoritative_path).iloc[:, 1]

    # Make a plot to compare the raw traces
    fig = mpl.figure.Figure(figsize=(7, 4))

    # Plot the raw traces
    ax_eeg = fig.add_subplot(1, 1, 1)
    ax_eeg.plot(authoritative_eeg.iloc[0:range_max], label="Reference implementation")
    ax_eeg.plot(eeg.iloc[0:range_max], label="This implementation", linestyle="--")
    ax_eeg.legend()
    fig.savefig(f"intermediate/chan_{chan_number}_comparison.png")


def compare_to_notebook_detections(detections, chan_number):
    """
    Compare the outputs of template-based detection from this implementation and the
    original aied notebook.

    Parameters:
    -----------
    detections: list
        A list containing sample numbers of detections

    chan_number: int
        The channel number

    """

    authoritative_path = "sample_templatespikes.hdf"
    authoritative_spikes = pd.read_hdf(authoritative_path, "spikes")
    auth_start_times = authoritative_spikes.query(f"channel == {chan_number}").apply(
        lambda x: x.spikeTime[0], axis=1
    )

    auth_set = set(auth_start_times)
    det_set = set(detections)

    print(f"Number of IEDs in both: {len(auth_set & det_set)}")
    print(f"Number of IEDs not in both: {len(auth_set ^ det_set)}")


def dataCleaner(df, samp_freq=200, win=3):
    """
    Adapted from the reference notebook, this method combines IEDs that occurred close
    together into a single detection.

    """

    bins = np.arange(df.samp_num.min(), df.samp_num.max(), samp_freq * win)
    spikebins = np.digitize(df.samp_num, bins)

    start_times = df.groupby(spikebins)["samp_num"].mean().astype(int)
    chans = df.groupby(spikebins)["channel"].apply(lambda x: list(x.unique()))

    final_df = pd.DataFrame(
        {
            "channel": chans,
            "samp_num": start_times,
        }
    )

    final_df = final_df[final_df.channel.apply(len) < 12]

    final_df = final_df.explode("channel")

    return final_df


def compare_results(ieds):
    """
    Compare the results of this implementation to the results of the original
    implementation.

    """
    auth_ieds = pd.read_csv("tester1_finalspikes.csv")
    # Loading from a CSV turns the lists in the channels column into strings. Convert
    # them back to lists of numbers.
    auth_ieds.loc[:, "channels"] = auth_ieds.loc[:, "channels"].apply(
        lambda x: [y for y in x.split("'") if y.isnumeric()]
    )
    auth_ieds = auth_ieds.explode("channels")

    valid_ieds = ieds.loc[ieds.is_valid == 1]

    # Make the two dataframes have the same column names so that we can merge them
    valid_ieds = valid_ieds.reset_index()
    valid_ieds = valid_ieds.rename({"level_0": "channel"}, axis=1).loc[
        :, ["channel", "samp_num"]
    ]
    auth_ieds = auth_ieds.rename(
        {"channels": "channel", "spikeStart": "samp_num"}, axis=1
    ).loc[:, ["channel", "samp_num"]]

    # Fix the types
    auth_ieds.channel = auth_ieds.channel.astype(int)
    valid_ieds.samp_num = valid_ieds.samp_num.astype(int)

    # Run data cleaning on the detections from this implementation so that it matches
    # the operations that were done to the reference implementation
    valid_ieds = dataCleaner(valid_ieds)

    # Merge and generate an indicator column "_merge", which inidicates whether each row
    # was in "left_only", meaning that it was a false negative; "right_only", meaning
    # that it was a false positive; or "both", meaning that my program detected a spike
    # that was also detected by the reference implementation.
    correspondence = pd.merge(
        auth_ieds, valid_ieds, on=["channel", "samp_num"], indicator=True, how="outer"
    )
    confusion = correspondence.groupby("_merge").count()
    confusion.rename(
        {"left_only": "false_neg", "right_only": "false_pos", "both": "true_pos"},
        inplace=True,
    )
    stats = confusion.loc[:, "samp_num"]
    stats.name = "stats"
    stats.loc["recall"] = stats.loc["true_pos"] / (
        stats.loc["false_neg"] + stats.loc["true_pos"]
    )
    stats.loc["precision"] = stats.loc["true_pos"] / (
        stats.loc["false_pos"] + stats.loc["true_pos"]
    )
    print(stats)


if __name__ == "__main__":

    sample_eeg, sample_labels = load_sample_data()
    ieds = find_sample_spikes(sample_eeg, 200)
    compare_results(ieds)
