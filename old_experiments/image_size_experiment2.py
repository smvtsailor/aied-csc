# Doing the same thing as image_size_experiment.ipynb, but outside of the jupyterlab
# environment so that the problem is reproducible.

import scipy.signal
import io
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import PIL

mpl.use("agg")

# Import the reference image
reference = PIL.Image.open("reference_tester1_5085_1.png").convert("RGB")
ref_arr = np.array(reference)

# Import the data for generating images for this clip
eeg = pd.read_hdf("5085_eeg.hdf", "eeg")

# We are basing this script off of an IED that occurs at sample 5085
spikestart = 5085
# And we know that the sampling frequency in this data is 200Hz
samp_freq = 200

# DPI at which to export the spectrogram image
dpi_setting = 300

# The number of data points used in each block for the FFT.
Nfft = 128 * (samp_freq / 500)  # d: 128

# Height and width of the exported spectrogram image in inches
h = 3
w = 3

# Padding is 1 by default
padding = 1

### filter out line noise
b_notch, a_notch = scipy.signal.iirnotch(60.0, 30.0, samp_freq)
ecogclip = pd.Series(scipy.signal.filtfilt(b_notch, a_notch, eeg))

### trim eeg clip based on cushion
### mean imputation if missing indices
end = int(float((spikestart + int(float(padding * samp_freq)))))
start = int(float((spikestart - int(float(padding * samp_freq)))))
if end > max(ecogclip.index):
    temp = list(
        ecogclip[
            list(
                range(
                    spikestart - int(float(padding * samp_freq)),
                    max(ecogclip.index),
                )
            )
        ]
    )
    cushend = [np.mean(ecogclip)] * (end - max(ecogclip.index))
    temp = np.concatenate([temp, cushend])
elif start < min(ecogclip.index):
    temp = list(
        ecogclip[list(range(min(ecogclip.index), spikestart + padding * samp_freq))]
    )
    cushstart = [np.mean(ecogclip)] * (min(ecogclip.index) - start)
    temp = np.concatenate([cushstart, temp])
else:
    temp = np.array(
        ecogclip[
            list(
                range(
                    spikestart - int(float(padding * samp_freq)),
                    spikestart + int(float(padding * samp_freq)),
                )
            )
        ]
    )


def do_plot(bottom_adjust):
    ### PLOT AND EXPORT:
    plt.figure(figsize=(h, w))
    plt.specgram(
        temp,
        NFFT=int(Nfft),
        Fs=samp_freq,
        noverlap=int(Nfft / 2),
        detrend="linear",
        cmap="YlOrRd",
    )
    plt.xlim(0, padding * 2)
    plt.ylim(0, 100)
    plt.axis("off")
    plt.subplots_adjust(bottom=bottom_adjust)

    imgdata = io.BytesIO()
    plt.savefig(imgdata, dpi=dpi_setting, format="png", transparent=True)
    # plt.savefig(f"results/{start}.png", dpi=dpi_setting, transparent=True)
    plt.close()

    img = PIL.Image.open(imgdata).convert("RGB")
    img_arr = np.array(img)
    return img_arr


img_arr = do_plot(0.125)

img_whitepx = np.sum(np.all(np.all(img_arr == 255, axis=2), axis=1))
ref_whitepx = np.sum(np.all(np.all(ref_arr == 255, axis=2), axis=1))
img_height = img_arr.shape[1]
ref_height = ref_arr.shape[1]
print(f"New Implementation: {img_whitepx}/{img_height} white rows")
print(f"Ref Implementation: {ref_whitepx}/{ref_height} white rows")
