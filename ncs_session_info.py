# NCS Session Info
# =============================================================
#
# This program is a command line utility to output the distribution of starting
# timestamps and sampling rates for each NCS file in a session.
#


import argparse
import sys
import os
import logging
import pandas as pd
import numpy as np
import glob

import neuralynx as nlx

import warnings
from torch.serialization import SourceChangeWarning

logger = logging.getLogger(__name__)


def collect_session_info(source_paths):
    """
    Collect information about the different NCS files within a list of sessions

    Parameters
    ----------
    source_paths: list
        A list of paths to NSC files.

    """
    # Deal with wildcard characters if there are any
    expanded_source_paths = []
    for source in source_paths:
        paths = glob.glob(source)
        if len(paths):
            expanded_source_paths.extend(paths)
        logger.debug(f"Expanded {source} to {paths}")

    source_dfs = []
    for source in expanded_source_paths:
        source_info = get_details_for_session(source)
        source_dfs.append(source_info)
    all_info = pd.concat(source_dfs)

    print("Counts by Start Time")
    print(all_info.groupby(["session", "start_time"]).count().sampling_rate)

    print("Counts by Sampling Rate")
    print(all_info.groupby(["session", "sampling_rate"]).count().start_time)

    print("Counts by Number of Samples")
    print(all_info.groupby(["session", "num_samples"]).count().start_time)


def get_details_for_session(source):
    """
    Detect session information for a single session/file.

    Parameters
    ----------
    source_path: list
        A path to a folder containing NSC files from the same session in which to detect
        IEDs, or to a single NSC file.

    Returns
    -------
    pd.DataFrame
        A DataFrame containing a row for each IED, and columns for the file name,
        channel number, and timestamp.

    """
    if not os.path.isdir(source):
        file_list = [source]
    else:
        file_list = []
        for root, _, filenames in os.walk(source):
            for filename in filenames:
                extension = os.path.splitext(filename)[-1].upper()
                if extension == ".NCS":
                    file_list.append(os.path.join(root, filename))

    session_info = pd.DataFrame(
        columns=["session", "start_time", "sampling_rate", "num_samples"]
    )
    for ncs_file in file_list:

        # Load the CSC file
        logger.info(f"Loading {ncs_file} for IED detection")
        session_info.loc[ncs_file, "session"] = source

        try:
            eeg, headers = nlx.load_ncs_for_sorting(ncs_file)
        except IndexError as exc:
            logger.warning(f"File {ncs_file} is invalid.")
            session_info.loc[ncs_file, "start_time"] = np.nan
            session_info.loc[ncs_file, "sampling_rate"] = np.nan
            session_info.loc[ncs_file, "num_samples"] = 0
            continue

        session_info.loc[ncs_file, "start_time"] = eeg.index.values[0]
        session_info.loc[ncs_file, "sampling_rate"] = int(
            float(headers["SamplingFrequency"])
        )
        session_info.loc[ncs_file, "num_samples"] = len(eeg.index.values)

    return session_info


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbosity", action="count", default=0)
    parser.add_argument(
        "source",
        nargs="+",
        help="Filepath(s) to the source Neuralynx CSC files to read. If a directory "
        "is provided, any sub-directories will be assumed to contain electrodes from "
        "the same session, and a common average re-reference will be applied.",
    )

    args = parser.parse_args(sys.argv[1:])

    # Configure logging to output with the requested verbosity
    if args.verbosity >= 3:
        log_level = logging.DEBUG
    elif args.verbosity == 2:
        log_level = logging.INFO
    elif args.verbosity == 1:
        log_level = logging.WARNING
    else:
        log_level = logging.CRITICAL
    logger.setLevel(log_level)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    # Adjust the log level for the neuralynx logger, which will otherwise spam warning
    # messages about problems with the timestamps
    nlx_logger = logging.getLogger("neuralynx")
    nlx_logger.setLevel(logging.CRITICAL)

    # Run the IED detection
    collect_session_info(
        args.source,
    )
