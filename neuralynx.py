# Tools to work with Neuralynx files. I suspect that this code actually came
# from this repo originally: https://github.com/alafuzof/NeuralynxIO And so
# here's the license:

# The MIT License (MIT) Copyright (c) 2015 Aleksander Alafuzoff Permission is
# hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the
# Software without restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions: The above copyright notice and this
# permission notice shall be included in all copies or substantial portions of
# the Software.  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
# EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
# OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# I've modified this file to add a convenience function to load the data into
# a pandas series and fix the timestamps/scaling. Also logging.
# - Stephen Meisenhelter


import os
import numpy as np
import pandas as pd
import datetime
import logging

logger = logging.getLogger(__name__)

HEADER_LENGTH = 16 * 1024  # 16 kilobytes of header

NCS_SAMPLES_PER_RECORD = 512
NCS_RECORD = np.dtype(
    [
        (
            "TimeStamp",
            np.uint64,
        ),  # Cheetah timestamp for this record. This corresponds to
        # the sample time for the first data point in the Samples
        # array. This value is in microseconds.
        (
            "ChannelNumber",
            np.uint32,
        ),  # The channel number for this record. This is NOT the A/D
        # channel number
        (
            "SampleFreq",
            np.uint32,
        ),  # The sampling frequency (Hz) for the data stored in the
        # Samples Field in this record
        (
            "NumValidSamples",
            np.uint32,
        ),  # Number of values in Samples containing valid data
        ("Samples", np.int16, NCS_SAMPLES_PER_RECORD),
    ]
)  # Data points for this record. Cheetah
# currently supports 512 data points per
# record. At this time, the Samples
# array is a [512] array.

NEV_RECORD = np.dtype(
    [
        ("stx", np.int16),  # Reserved
        ("pkt_id", np.int16),  # ID for the originating system of this packet
        ("pkt_data_size", np.int16),  # This value should always be two (2)
        ("TimeStamp", np.uint64),  # Cheetah timestamp for this record. This value is in
        # microseconds.
        ("event_id", np.int16),  # ID value for this event
        ("ttl", np.int16),  # Decimal TTL value read from the TTL input port
        ("crc", np.int16),  # Record CRC check from Cheetah. Not used in consumer
        # applications.
        ("dummy1", np.int16),  # Reserved
        ("dummy2", np.int16),  # Reserved
        (
            "Extra",
            np.int32,
            8,
        ),  # Extra bit values for this event. This array has a fixed
        # length of eight (8)
        ("EventString", "S", 128),
    ]
)  # Event string associated with this event record. This string
# consists of 127 characters plus the required null termination
# character. If the string is less than 127 characters, the
# remainder of the characters will be null.

VOLT_SCALING = (1, "V")
MILLIVOLT_SCALING = (1000, "mV")
MICROVOLT_SCALING = (1000000, "µV")


def read_header(fid):
    # Read the raw header data (16 kb) from the file object fid. Restores the position in the file object after reading.
    pos = fid.tell()
    fid.seek(0)
    raw_hdr = fid.read(HEADER_LENGTH).strip(b"\0")
    fid.seek(pos)

    return raw_hdr


def parse_header(raw_hdr):
    # Parse the header string into a dictionary of name value pairs
    hdr = dict()

    # Decode the header as iso-8859-1 (the spec says ASCII, but there is at least one case of 0xB5 in some headers)
    raw_hdr = raw_hdr.decode("iso-8859-1")

    # Neuralynx headers seem to start with a line identifying the file, so
    # let's check for it
    hdr_lines = [line.strip() for line in raw_hdr.split("\r\n") if line != ""]
    if hdr_lines[0] != "######## Neuralynx Data File Header":
        logger.warning("Unexpected start to header: " + hdr_lines[0])

    # Try to read the original file path
    try:
        assert hdr_lines[1].split()[1:3] == ["File", "Name"]
        hdr["FileName"] = " ".join(hdr_lines[1].split()[3:])
        # hdr['save_path'] = hdr['FileName']
    except:
        logger.warning(
            "Unable to parse original file path from Neuralynx header: " + hdr_lines[1]
        )

    # Process lines with file opening and closing times
    hdr["TimeOpened"] = hdr_lines[2][3:]
    hdr["TimeOpened_dt"] = parse_neuralynx_time_string(hdr_lines[2])
    hdr["TimeClosed"] = hdr_lines[3][3:]
    hdr["TimeClosed_dt"] = parse_neuralynx_time_string(hdr_lines[3])

    # Read the parameters, assuming "-PARAM_NAME PARAM_VALUE" format
    for line in hdr_lines[4:]:
        try:
            name, value = line[
                1:
            ].split()  # Ignore the dash and split PARAM_NAME and PARAM_VALUE
            hdr[name] = value
        except:
            logger.warning(
                "Unable to parse parameter line from Neuralynx header: " + line
            )

    return hdr


def read_records(fid, record_dtype, record_skip=0, count=None):
    # Read count records (default all) from the file object fid skipping the first record_skip records. Restores the
    # position of the file object after reading.
    if count is None:
        count = -1

    pos = fid.tell()
    fid.seek(HEADER_LENGTH, 0)
    fid.seek(record_skip * record_dtype.itemsize, 1)
    rec = np.fromfile(fid, record_dtype, count=count)
    fid.seek(pos)

    return rec


def estimate_record_count(file_path, record_dtype):
    # Estimate the number of records from the file size
    file_size = os.path.getsize(file_path)
    file_size -= HEADER_LENGTH

    if file_size % record_dtype.itemsize != 0:
        logger.warning(
            "File size is not divisible by record size (some bytes unaccounted for)"
        )

    return file_size / record_dtype.itemsize


def parse_neuralynx_time_string(time_string):
    # Parse a datetime object from the idiosyncratic time string in Neuralynx file headers
    try:
        tmp_date = [int(x) for x in time_string.split()[4].split("/")]
        tmp_time = [
            int(x) for x in time_string.split()[-1].replace(".", ":").split(":")
        ]
        tmp_microsecond = tmp_time[3] * 1000
    except:
        logger.warning(
            "Unable to parse time string from Neuralynx header: " + time_string
        )
        return None
    else:
        return datetime.datetime(
            tmp_date[2],
            tmp_date[0],
            tmp_date[1],  # Year, month, day
            tmp_time[0],
            tmp_time[1],
            tmp_time[2],  # Hour, minute, second
            tmp_microsecond,
        )


def check_ncs_records(records):
    # Check that all the records in the array are "similar" (have the same sampling frequency etc.
    dt = np.diff(records["TimeStamp"])
    dt = np.abs(dt - dt[0])
    if not np.all(records["ChannelNumber"] == records[0]["ChannelNumber"]):
        logger.warning("Channel number changed during record sequence")
        return False
    elif not np.all(records["SampleFreq"] == records[0]["SampleFreq"]):
        logger.warning("Sampling frequency changed during record sequence")
        return False
    elif not np.all(records["NumValidSamples"] == 512):
        logger.warning("Invalid samples in one or more records")
        return False
    elif not np.all(dt <= 1):
        logger.warning("Time stamp difference tolerance exceeded")
        return False
    else:
        return True


def load_ncs(
    file_path, load_time=True, rescale_data=True, signal_scaling=MICROVOLT_SCALING
):
    # Load the given file as a Neuralynx .ncs continuous acquisition file and extract the contents
    file_path = os.path.abspath(file_path)
    with open(file_path, "rb") as fid:
        raw_header = read_header(fid)
        records = read_records(fid, NCS_RECORD)

    header = parse_header(raw_header)
    check_ncs_records(records)

    # Reshape (and rescale, if requested) the data into a 1D array
    # data = records['Samples'].ravel()

    data = records["Samples"]
    # data = records['Samples'].reshape((NCS_SAMPLES_PER_RECORD * len(records), 1))
    if rescale_data:
        try:
            # ADBitVolts specifies the conversion factor between the ADC counts and volts
            data = data.astype(np.float64) * (
                np.float64(header["ADBitVolts"]) * signal_scaling[0]
            )
        except KeyError:
            logger.warning(
                "Unable to rescale data, no ADBitVolts value specified in header"
            )
            rescale_data = False

    # Pack the extracted data in a dictionary that is passed out of the function
    ncs = dict()
    ncs["file_path"] = file_path
    ncs["raw_header"] = raw_header
    ncs["header"] = header
    ncs["data"] = data
    ncs["data_units"] = signal_scaling[1] if rescale_data else "ADC counts"
    ncs["sampling_rate"] = records["SampleFreq"][0]
    ncs["channel_number"] = records["ChannelNumber"][0]
    ncs["timestamp"] = records["TimeStamp"]

    # Calculate the sample time points (if needed)
    if load_time:
        num_samples = data.shape[0]
        times = np.interp(
            np.arange(num_samples), np.arange(0, num_samples, 512), records["TimeStamp"]
        ).astype(np.uint64)
        ncs["time"] = times
        ncs["time_units"] = "µs"

    return ncs


def load_nev(file_path):
    # Load the given file as a Neuralynx .nev event file and extract the contents
    file_path = os.path.abspath(file_path)
    with open(file_path, "rb") as fid:
        raw_header = read_header(fid)
        records = read_records(fid, NEV_RECORD)

    header = parse_header(raw_header)

    # Check for the packet data size, which should be two. DISABLED because these seem to be set to 0 in our files.
    # assert np.all(record['pkt_data_size'] == 2), 'Some packets have invalid data size'

    # Pack the extracted data in a dictionary that is passed out of the function
    nev = dict()
    nev["file_path"] = file_path
    nev["raw_header"] = raw_header
    nev["header"] = header
    nev["records"] = records
    nev["events"] = records[
        ["pkt_id", "TimeStamp", "event_id", "ttl", "Extra", "EventString"]
    ]

    return nev


def load_ncs_for_sorting(file_path):
    """
    Function to load a CSC stream from a NCS file, rescale the data to uV, and
    calculate sample timestamps. This function was not part of the original
    file and was added by Stephen Meisenhelter.

    Parameters
    ----------
    file_path: str
        Path to an NCS file to open.

    Returns
    -------
    [data (pd.Series), headers (dict)]
        Returns a pandas series with data in uV and the index as POSIX
        timestamp in seconds since epoch. Headers is a dictionary
        containing information about the NCS file.

    """
    logger.info(f"Loading data from {file_path}")
    ncs_data = load_ncs(file_path, rescale_data=False, load_time=False)
    logger.debug(f"Found data in {file_path}")

    data = ncs_data["data"]
    header = ncs_data["header"]
    sampling_rate = int(float(header["SamplingFrequency"]))
    scaling_factor = float(header["ADBitVolts"])
    logger.info(f"Sampling rate: {sampling_rate}")
    logger.info(f"Scaling factor: {scaling_factor}")

    timestamps = ncs_data["timestamp"]

    # Just to record it in the log, detect gaps in the EEG record with a quick
    # and dirty check of the CV of the timestamps diff. We'll assume that if there's more than 0.1% variation, there is a gap in the file.
    ts_diff = np.diff(timestamps)
    ts_cv = np.std(ts_diff) / np.mean(ts_diff)
    logger.debug(
        f"Coefficient of variation in EEG timestamps is " f"{ts_cv*100:0.1f}\%"
    )
    if ts_cv > 1e-3:
        logger.info("Detected gap in EEG record!")

    # The NCS file is stored as a series of (usually 512 sample long) records.
    # We get one timestamp per record, so we figure out the timestamp for the
    # individual samples by adding multiples of the sampling duration to each
    # sample within a record.
    record_length = ncs_data["data"].shape[1]
    sample_duration_us = 1e6 / sampling_rate
    sample_offsets = np.arange(0, record_length) * sample_duration_us

    # Apply the sample offsets to each record and concatenate the records
    sample_timestamps = np.ravel(np.vstack(timestamps) + sample_offsets)

    # Make the data one dimensional to match the timestamps
    data = data.ravel()

    # Scale the data to microvolts. The 1e6 factor here converts from volts to
    # microvolts.
    data_uV = data * scaling_factor * 1e6

    # Put the data into a Pandas series for easy manipulation
    data_series = pd.Series(data_uV, index=sample_timestamps)
    return [data_series, header]


if __name__ == "__main__":
    file_path = "data/rlab_ncs_samples/102/raw/CSC41.ncs"
    data, header = load_ncs_for_sorting(file_path)
    print(f"Number of samples: {len(data)}")
    print(f'Sampling rate: {header["SamplingFrequency"]}')
    print(f"Start time: {datetime.datetime.fromtimestamp(data.index[0]/1e6)}")
    print(f"TS End time: {datetime.datetime.fromtimestamp(data.index[-1]/1e6)}")
    calculated_duration = len(data) / int(header["SamplingFrequency"]) * 1e6
    calc_end = data.index[0] + calculated_duration
    print(f"Calc End Time: {datetime.datetime.fromtimestamp(calc_end/1e6)}")
