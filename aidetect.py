# Functions to run the AI-based component of the aIED system. To run the full detector,
# use detect-ieds.py instead.

import torch
import logging
import scipy.signal
import io
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import PIL
import warnings

from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from torch.serialization import SourceChangeWarning


mpl.use("Agg")

warnings.filterwarnings("ignore", category=SourceChangeWarning)
logger = logging.getLogger(__name__)

model = torch.load("model_aied.pt")


def aidetect(eeg, prospective_starts, samp_freq):
    """
    Run AI IED detection to confirm whether prospective IEDs are real IEDs.

    Parameters
    ----------
    eeg: pd.Series
        A single channel EEG signal, with timestamps as the index

    start:
        A sample number in the EEG index at which the IED starts

    samp_freq: float
        The sampling frequency in Hertz

    Returns
    -------
    A pandas DataFrame with a timestamp column and a "is_valid" column to indicate
    whether each prospective IED is a real IED.

    """
    # Generate spectrogram images
    specs = []
    logger.info(f"Generating {len(prospective_starts)} spectrogram images")
    for start in prospective_starts:
        # Get the input to the classifier
        spec = spectrogram(eeg, start, samp_freq)
        specs.append(spec)

    # Prepare an output dataframe
    results = pd.DataFrame(columns=["samp_num", "is_valid", "source"])

    # Run the images through the classifier
    if len(specs) > 0:
        dispositions = ai_disposition(specs)

    # Generate the results dataframe
    for i, start in enumerate(prospective_starts):
        results.loc[i, "samp_num"] = start
        results.loc[i, "is_valid"] = 1 - dispositions[i]

    return results


def spectrogram(eeg, spikestart, samp_freq, padding=1):
    """
    Generates a spectrogram based on a segment of single channel EEG data.

    Parameters
    ----------
    eeg: pd.Series
        A single channel EEG signal, with timestamps as the index

    spikestart:
        A sample number in the EEG index at which the IED starts

    samp_freq: float
        The sampling frequency in Hertz

    padding: float, optional
        The duration in seconds of the padding on either side of the spike start time
        which is used to calculate the histogram.

    Returns
    -------
    A file object containing a matplotlib image of the spectrogram which is suitable to
    feed to the CNN classifier.

    """

    # This function is the way that it is because this was how the original aIED paper
    # generated images for the input to the pretrained model. Since proving the
    # correctness of a new set of inputs to the pretrained model is out of scope for
    # this project, I'm just modifying this so that it doesn't actually save the image
    # file anywhere and instead just keeps it in memory. Otherwise this is pretty much
    # as it appeared in the ipynb that started this whole project.

    # DPI at which to export the spectrogram image
    dpi_setting = 300

    # The number of data points used in each block for the FFT. From the documentation
    # of matploblib.pyplot.specgram. I'm assuming that we're dividing by 500 because
    # Robert originally hard-coded this to use a 500Hz sampling rate, and then realized
    # that he would need to scale the actual sampling rate to work with the existing
    # code.
    Nfft = 128 * (samp_freq / 500)  # d: 128

    # Height and width of the exported spectrogram image in inches
    h = 3
    w = 3

    ### filter out line noise
    b_notch, a_notch = scipy.signal.iirnotch(60.0, 30.0, samp_freq)
    ecogclip = pd.Series(scipy.signal.filtfilt(b_notch, a_notch, eeg))

    ### trim eeg clip based on cushion
    ### mean imputation if missing indices
    end = int(float((spikestart + int(float(padding * samp_freq)))))
    start = int(float((spikestart - int(float(padding * samp_freq)))))
    if end > max(ecogclip.index):
        temp = list(
            ecogclip[
                list(
                    range(
                        spikestart - int(float(padding * samp_freq)),
                        max(ecogclip.index),
                    )
                )
            ]
        )
        cushend = [np.mean(ecogclip)] * (end - max(ecogclip.index))
        temp = np.concatenate([temp, cushend])
    elif start < min(ecogclip.index):
        temp = list(
            ecogclip[list(range(min(ecogclip.index), spikestart + padding * samp_freq))]
        )
        cushstart = [np.mean(ecogclip)] * (min(ecogclip.index) - start)
        temp = np.concatenate([cushstart, temp])
    else:
        temp = np.array(
            ecogclip[
                list(
                    range(
                        spikestart - int(float(padding * samp_freq)),
                        spikestart + int(float(padding * samp_freq)),
                    )
                )
            ]
        )

    ### PLOT AND EXPORT:
    plt.figure(figsize=(h, w))
    plt.specgram(
        temp,
        NFFT=int(Nfft),
        Fs=samp_freq,
        noverlap=int(Nfft / 2),
        detrend="linear",
        cmap="YlOrRd",
    )
    plt.xlim(0, padding * 2)
    plt.ylim(0, 100)
    plt.axis("off")

    # Set the bottom margin. This matches the setting for the Matplotlib inline backend
    # that was used in the original ipynb implementation.
    plt.subplots_adjust(bottom=0.125)

    imgdata = io.BytesIO()
    plt.savefig(imgdata, dpi=dpi_setting, format="png", transparent=True)
    # plt.savefig(f"results/{start}.png", dpi=dpi_setting, transparent=True)
    plt.close()

    return imgdata


class SpecImageLoader(Dataset):
    """
    An image loader for spectrogram images.
    """

    def __init__(self, img_files):
        # Open each PNG image and convert it to a numpy array. Then stack the arrays.
        img_arrays = []
        for img_file in img_files:
            img = PIL.Image.open(img_file).convert("RGB")
            img_arrays.append(np.array(img))
        self.images = np.stack(img_arrays, axis=0)

        # Define a pipeline for transforming the images for the classifier. This is
        # straight from the aIED paper, and changing it will require retraining the
        # model.
        self.transform = transforms.Compose(
            [
                transforms.ToPILImage(),
                transforms.Resize(224),
                transforms.Pad(1, fill=0, padding_mode="constant"),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
            ]
        )

    def __len__(self):
        return self.images.shape[0]

    def __getitem__(self, index):
        return self.transform(self.images[index])


def ai_disposition(img_files):
    """
    Take spectrogram images and return whether they are from IEDs.

    Parameters
    ----------
    img_files: file-object
        A list of images of spectrograms of prospective IEDs

    Returns
    -------
    A boolean disposition which is True if the prospective IED is a real IED.
    """

    # Load up the images that we were passed
    logger.info("Converting spectrograms for classifier")
    dataset = SpecImageLoader(img_files)
    data = DataLoader(
        dataset,
        batch_size=len(img_files),
        shuffle=False,
        num_workers=1,
        pin_memory=True,
    )

    logger.info("Starting classification")
    # Run the images through the model and collect the results
    dispositions = []
    with torch.no_grad():
        for i, inputs in enumerate(data):
            logger.debug(f"Inside classifier batch {i}")

            # These lines are useful for plotting the input to the model.
            # recon_img = transforms.ToPILImage()(inputs[0, :, :, :])
            # recon_img.show()

            outputs = model.forward(inputs)
            _, predicted = torch.max(outputs, 1)
            pred = predicted.numpy()
            dispositions.append(pred)
    logger.debug("Finished classification")

    return np.concatenate(dispositions, 0)
