# aIED: An automated interictal epileptiform discharge detector
# =============================================================
#
# This program is a command line utility to output the timestamps of interictal
# discharges that occur in a provided CSC file.
#
# The core of the program is a two-stage IED detection algorithm originally implemented
# at Dartmouth College by Quon et al. (https://doi.org/10.1016/j.clinph.2021.09.018).
#
# Run `detect-ieds.py -h` to print the usage message.

import argparse
import sys
import os
import pathlib
import logging
import torch.multiprocessing as multiprocessing
import pandas as pd
import numpy as np
import spikedetector
import utils
import glob
from functools import partial
from matplotlib.figure import Figure

import aidetect
import neuralynx as nlx

import warnings
from torch.serialization import SourceChangeWarning

logger = logging.getLogger(__name__)


class InvalidNCSError(Exception):
    pass


class InconsistentSessionError(Exception):
    pass


def detect_ieds_for_files(source_paths, dest_path, n_threads=1, skip_empty=False):
    """
    Detect IEDs in the specified source paths and outputs a CSV file to the specified
    destination path containing records of IED timestamps from all of the source files.

    Parameters
    ----------
    source_paths: list
        A list of paths to CSC files in which to detect IEDs.

    dest_path: str
        A path to which a CSV file containing IED timestamps will be written.

    n_threads: int
        The number of threads to be used for processing. If greater than 1, the members
        of source_paths will be distributed to separate threads for simultaneous
        processing.

    skip_empty: bool
        If True, empty sources will be skipped and and execution will continue. If
        False, encountering an empty source will raise an error.

    Returns
    -------
    None

    """
    # Deal with wildcard characters if there are any
    expanded_source_paths = []
    for source in source_paths:
        paths = glob.glob(source)
        if len(paths):
            expanded_source_paths.extend(paths)
        logger.debug(f"Expanded {source} to {paths}")

    if len(expanded_source_paths) == 0:
        logger.warning("No input files found")
        ieds = pd.DataFrame(columns=["timestamp", "is_valid", "path", "session"])

    elif n_threads == 1:
        ieds = []
        for source in expanded_source_paths:
            file_ieds = detect_ieds_in_session(source, not skip_empty)
            ieds.append(file_ieds)
    else:
        with multiprocessing.Pool(n_threads) as pool:
            diis = partial(detect_ieds_in_session, error_on_read_fail=not skip_empty)
            ieds = pool.map(diis, expanded_source_paths)

    if len(ieds) > 0:
        ieds = pd.concat(ieds)

    valid_ieds = ieds[ieds.is_valid == 1]
    valid_ieds.drop("is_valid", axis=1, inplace=True)
    valid_ieds.index.name = "id"
    logger.debug(f"Writing CSV output to {dest_path}")
    valid_ieds.to_csv(dest_path)
    logger.info(f"Wrote CSV output to {dest_path}")


def detect_ieds_in_session(source, error_on_read_fail=True):
    """
    Detect IEDs in the specified source path and outputs a DataFrame contining IED
    timestamp information.

    Parameters
    ----------
    source_path: list
        A path to a folder containing NSC files from the same session in which to detect
        IEDs, or to a single NSC file.

    error_on_read_fail: bool
        If True, an error will be raised if an NCS file cannot be read. Otherwise an
        empty dataframe will be returned.

    Returns
    -------
    pd.DataFrame
        A DataFrame containing a row for each IED, and columns for the file name,
        channel number, and timestamp.

    """
    if not os.path.isdir(source):
        file_list = [source]
    else:
        file_list = []
        for root, _, filenames in os.walk(source):
            for filename in filenames:
                extension = os.path.splitext(filename)[-1].upper()
                if extension == ".NCS":
                    file_list.append(os.path.join(root, filename))
    sess_stem = pathlib.PurePath(source).stem
    all_eeg = {}
    session_samp_rate = None

    # Return an empty dataframe if there are no files in the file list
    if len(file_list) == 0:
        return pd.DataFrame(columns=["timestamp", "is_valid", "path", "session"])

    for ncs_file in file_list:
        # Load the CSC file
        logger.info(f"Loading {ncs_file} for IED detection")
        try:
            eeg, headers = nlx.load_ncs_for_sorting(ncs_file)
        except IndexError as exc:
            if error_on_read_fail:
                raise InvalidNCSError(f"{ncs_file} is invalid.")
            else:
                logger.warning(f"File {ncs_file} is invalid.")
                continue

        # If this is the first file we're loading from this session, use it to make sure
        # that all of the other files have the same sampling rate, since
        # we're applying a common average reference.
        samp_rate = int(float(headers["SamplingFrequency"]))
        if session_samp_rate is None:
            session_samp_rate = samp_rate
        elif samp_rate != session_samp_rate:
            if error_on_read_fail:
                raise InconsistentSessionError(
                    f"{sess_stem} contains NCS files with an "
                    "inconsistent sampling rate."
                )
            else:
                logger.warning(
                    f"Session {sess_stem} is invalid due to sampling rate "
                    f"mismatch in {ncs_file}."
                )
                return pd.DataFrame(
                    columns=["timestamp", "is_valid", "path", "session"]
                )

        all_eeg[ncs_file] = eeg

    all_eeg = pd.DataFrame(all_eeg)
    if len(all_eeg) == 0:
        return pd.DataFrame(columns=["timestamp", "is_valid", "path", "session"])

    # One day we need to look into what's happening when the channels start at different times
    # plot_retained_data(all_eeg, f"results/{sess_stem}_alignment.png")

    # Remove any samples that don't exist for all channels
    pre_align_len = len(all_eeg)
    all_eeg.dropna(inplace=True)
    post_align_len = len(all_eeg)
    remove_perc = (pre_align_len - post_align_len) / pre_align_len * 100
    logger.info(f"Dropped {remove_perc:.1f}% of samples for alignment")

    # Do common average referencing
    logger.info(f"Applying common average rereferencing to {source}")
    all_eeg = (all_eeg.T - all_eeg.mean(axis=1)).T

    all_ieds = []
    for path, eeg in all_eeg.iteritems():
        logger.info(f"Running full detector on {path}")
        ieds = run_full_detector(eeg, session_samp_rate)
        if len(ieds) > 0:
            # Add a record of the filename that this came from
            ieds.loc[:, "path"] = path
            ieds.loc[:, "session"] = source
        all_ieds.append(ieds)

    return pd.concat(all_ieds)


def plot_retained_data(eeg, filename):
    """
    Plot which sections of data will be removed due to null values.

    """

    samp_retained = ~eeg.isnull().any(axis=1)
    fig = Figure(figsize=(7, 3.5))
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(samp_retained.index, samp_retained.values)
    fig.savefig(filename, dpi=300)


def run_full_detector(eeg, samp_rate):
    """
    Run the full two-stage IED detector on a series containing ECoG data.

    Parameters
    ----------
    eeg: pd.Series
        A time series of EEG data with timestamps in the index

    samp_rate: float
        The number of samples per second in the eeg timeseries

    Returns
    -------
    pd.DataFrame
        A dataframe containing timestamps for each detected IED

    """

    eeg = utils.preprocess(eeg, samp_rate)

    # The preprocessing routine may downsample data, so update the sampling rate
    samp_rate = int(np.round(1e6 / np.median(np.diff(eeg.index.values))))

    # Run template matching detection
    logger.info("Running template-based detector")
    # Note that spikedetector.detect.detect will return the sample number, not the
    # timestamp, associated with the start and end of each IED.
    prospectives = spikedetector.detect.detect(eeg, samp_rate)
    logger.info(f"Found {len(prospectives)} prospective IEDs")
    prospective_starts = [x[0] for x in prospectives]

    # Run the AI based component of the spike detector
    ieds = aidetect.aidetect(eeg, prospective_starts, samp_rate)

    # Add the timestamp based on the sample number
    start_time = eeg.index.values[0]
    # ieds.loc[:, "timestamp"] = ieds.samp_num / samp_rate * 1e6 + start_time
    ieds.loc[:, "timestamp"] = eeg.iloc[ieds.samp_num].index.values
    return ieds


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbosity", action="count", default=0)
    parser.add_argument(
        "-s",
        "--skip-empty",
        action="store_true",
        help=(
            "If true, skip over empty NCS files and continue processing remaining "
            "files."
        ),
    )
    parser.add_argument(
        "--n-threads",
        type=int,
        default=1,
        help=(
            "Number of threads to use for processing data. If >1, files will be "
            "distributed to separate worker processes."
        ),
    )
    parser.add_argument(
        "source",
        nargs="+",
        help="Filepath(s) to the source Neuralynx CSC files to read. If a directory "
        "is provided, any sub-directories will be assumed to contain electrodes from "
        "the same session, and a common average re-reference will be applied.",
    )
    parser.add_argument("output", nargs=1, help="Filepath to the output file ")

    args = parser.parse_args(sys.argv[1:])

    # Configure logging to output with the requested verbosity
    if args.verbosity >= 3:
        log_level = logging.DEBUG
    elif args.verbosity == 2:
        log_level = logging.INFO
    elif args.verbosity == 1:
        log_level = logging.WARNING
    else:
        log_level = logging.CRITICAL
    logger.setLevel(log_level)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    # Adjust the log level for the neuralynx logger, which will otherwise spam warning
    # messages about problems with the timestamps
    nlx_logger = logging.getLogger("neuralynx")
    nlx_logger.setLevel(logging.CRITICAL)

    # Likewise, ignore warnings from torch about how the source code for the model has
    # changed since the model was saved. As of now the changes don't seem to impact the
    # results. If it does, the original source is in the model as an attribute and the
    # torch patch tool can restore it.
    warnings.filterwarnings("ignore", category=SourceChangeWarning)

    aid_logger = logging.getLogger("aidetect")
    aid_logger.setLevel(log_level)

    # Run the IED detection
    detect_ieds_for_files(
        args.source,
        args.output[0],
        n_threads=args.n_threads,
        skip_empty=args.skip_empty,
    )
