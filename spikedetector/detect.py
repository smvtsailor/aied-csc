from scipy import signal
import numpy as np
from .detect_peaks import detect_peaks


def locate_downsample_freq(sample_freq, min_freq=200, max_freq=340):
    """
    Figure out an intelligent downsampling target to minimize the memory needed
    for resampling. Added by Stephen, originally down_sample_freq was always
    256 (set as a global var) in Peter's and Mark's implementation. Min and max
    freq defaults were chosen completely arbitrarily - not sure if there was
    originally a reason for wanting 256, but probably not.

    sample_freq: Existing sample rate

    min_freq: Minimum sampling rate

    max_freq: Maximum sampling rate

    """

    min_up_factor = np.inf
    best_candidate_freq = None
    for candidate in range(min_freq, max_freq + 1):
        down_samp_rate = sample_freq / float(candidate)
        down_factor, up_factor = down_samp_rate.as_integer_ratio()
        if up_factor <= min_up_factor:
            min_up_factor = up_factor
            best_candidate_freq = candidate

    return best_candidate_freq


def butter_bandpass(low_limit, high_limit, samp_freq, order=5):
    """
    Returns a Butterworth bandpass filter with the specified characteristics.

    """

    sos = signal.butter(
        order, [low_limit, high_limit], samp_freq, btype="band", output="sos"
    )

    def bb_filter(data):
        return signal.sosfiltfilt(sos, data)

    return bb_filter


def detect(channel, samp_freq, return_eeg=False, temp_func=None, signal_func=None):
    # assume that eeg is [channels x samples]

    # Round samp_freq to the nearest integer if it is large
    if samp_freq > 100:
        samp_freq = int(np.round(samp_freq))
    down_samp_freq = locate_downsample_freq(samp_freq)
    template = signal.triang(np.round(down_samp_freq * 0.06))
    kernel = np.array([-2, -1, 1, 2]) / float(8)
    template = np.convolve(kernel, np.convolve(template, kernel, "valid"), "full")
    if temp_func:
        template = temp_func(template, samp_freq)
    if signal_func:
        channel = signal_func(channel, samp_freq)

    down_samp_rate = samp_freq / float(down_samp_freq)
    down_samp_factor, up_samp_factor = down_samp_rate.as_integer_ratio()

    if down_samp_factor > 1:
        # Filtering added by Stephen, differs from Peter's and Mark's
        # implementations
        bb_filter = butter_bandpass(1, down_samp_freq, samp_freq)
        channel = bb_filter(channel)
        channel = signal.resample_poly(channel, up_samp_factor, down_samp_factor)
    channel = signal.detrend(channel, type="constant")
    results = template_match(channel, template, down_samp_freq)
    up_samp_results = [
        np.round(spikes * down_samp_factor / float(up_samp_factor)).astype(int)
        for spikes in results
    ]
    if return_eeg:
        return up_samp_results, [channel[start:end] for start, end in results]
    else:
        return up_samp_results


def template_match(channel, template, down_samp_freq, thresh=7, min_spacing=0):
    template_len = len(template)
    cross_corr = np.convolve(channel, template, "valid")
    cross_corr_std = med_std(cross_corr, down_samp_freq)

    detections = []
    # catch empty channels
    if cross_corr_std > 0:
        # normalize the cross-correlation
        cross_corr_norm = (cross_corr - np.mean(cross_corr)) / cross_corr_std
        cross_corr_norm[1] = 0
        cross_corr_norm[-1] = 0

        # find regions with high cross-corr
        if np.any(abs(cross_corr_norm > thresh)):
            peaks = detect_peaks(abs(cross_corr_norm), mph=thresh, mpd=template_len)
            peaks += int(np.ceil(template_len / 2.0))  # center detection on template
            peaks = [
                peak
                for peak in peaks
                if peak > template_len and peak <= len(channel) - template_len
            ]
            if peaks:
                # find peaks that are at least (min_spacing) secs away
                distant_peaks = np.diff(peaks) > min_spacing * down_samp_freq
                # always keep the first peak
                to_keep = np.insert(distant_peaks, 0, True)
                peaks = [peaks[x] for x in range(len(peaks)) if to_keep[x] == True]
                detections = [
                    (peak - template_len, peak + template_len) for peak in peaks
                ]

    return np.array(detections)


def med_std(signal, window_len):
    window = np.zeros(window_len) + (1 / float(window_len))
    std = np.sqrt(
        np.median(
            np.convolve(np.square(signal), window, "valid")
            - np.square(np.convolve(signal, window, "valid"))
        )
    )
    return std
