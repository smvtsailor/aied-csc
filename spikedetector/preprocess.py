import pandas as pd
import numpy as np
from scipy.signal import butter, lfilter

def notch_filter(data, sample_rate):
    after_sixty = sixty_hz_filter(data, sample_rate)
    after_onetwenty = onetwenty_hz_filter(after_sixty, sample_rate)
    if sample_rate > 360:
        after_oneeighty = oneeighty_hz_filter(after_onetwenty, sample_rate)
        return after_oneeighty
    else:
        return after_onetwenty

def sixty_hz_filter(data, sample_rate):
    return butter_band_filter(data, 'bandstop', 58., 62., sample_rate)

def onetwenty_hz_filter(data, sample_rate):
    return butter_band_filter(data, 'bandstop', 118., 122., sample_rate)

def oneeighty_hz_filter(data, sample_rate):
    return butter_band_filter(data, 'bandstop', 178., 182., sample_rate)

def butter_band(btype, lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype=btype)
    return b, a

def butter_band_filter(data, btype, lowcut, highcut, fs, order=5):
    b, a = butter_band(btype, lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def remove_artifactual_channels(session, exclude=[]):
    excluded = session.drop(exclude, axis=1)
    stds = np.std(excluded, axis=0)
    median = np.median(stds)
    # transform stds into a T/F array,
    # then cut the values of > 2x STD
    artifactual_channels = stds.index.where(stds > 2 * median).dropna()
    return len(artifactual_channels), session.drop(artifactual_channels, axis=1)

def common_average_rereference(session):
    return session.sub(session.mean(axis=1), axis=0)
